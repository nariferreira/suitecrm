<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'ERR_PHP'=>'%s[%d]: %s em %s on line %d',
'LBL_ASSIGNED_TO_ID'=>'Atribuído ao Usuário com Id',
'ERR_CURL'=>'CURL não instalado - não se pode rodar jobs via URL',
'LBL_DATA'=>'Data do Job',
'LBL_REQUEUE'=>'Em caso de Falha tentar novamente',
'ERR_TIMEOUT'=>'Falha forçada por timeout',
'ERR_FAILED'=>'Falha não esperada, favor checar os logs e erro e o arquivo sugarcrm.log',
'LBL_FAIL_COUNT'=>'Falhas',
'ERR_NOSUCHUSER'=>'ID %s de usuário não encontrado',
'LBL_INTERVAL'=>'Intervalo mínimo entre tentativas',
'ERR_JOB_FAILED_VERBOSE'=>'Job %1$s (%2$s) falhou na execução do CRON',
'LBL_MESSAGE'=>'Mensagens',
'LBL_RETRY_COUNT'=>'Máximo de Tentativas',
'ERR_NOUSER'=>'Nenhum ID de usuário especificado para o Job',
'LBL_NAME'=>'Nome do Job',
'ERR_CALL'=>'Não se pode chamar a função: %s',
'LBL_PERCENT'=>'Porcentagem completada',
'LBL_RESOLUTION'=>'Resultado',
'LBL_SCHEDULER_ID'=>'Scheduler',
'LBL_STATUS'=>'Status do Job',
'LBL_EXECUTE_TIME'=>'Tempo de Execução',
'ERR_JOBTYPE'=>'Tipo de tarefa desconhecida: %s',
'LBL_CLIENT'=>'Tornar-se proprietário do Cliente',
'LBL_ASSIGNED_TO_NAME'=>'Usuário',
);
?>