<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_ACCEPT_DATABASE'=>'Aceitar Base de Dados',
'LBL_ACCEPT_YOURS'=>'Aceitar o Seu',
'LBL_IN_DATABASE'=>'Na Base de Dados',
'LBL_NO_LOCKED_OBJECTS'=>'Nenhuns Objetos Bloqueados',
'LBL_RECORDS_MATCH'=>'Registros Coincidem',
'LBL_YOURS'=>'Seu',
'LBL_CONFLICT_EXISTS'=>'Um Conflito Existe Em -',
);
?>