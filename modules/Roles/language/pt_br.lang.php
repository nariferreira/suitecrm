<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_DESCRIPTION'=>'Descrição:',
'LBL_ASSIGN_MODULES'=>'Editar módulos: ',
'LBL_LANGUAGE'=>'Idioma: ',
'LBL_LIST_FORM_TITLE'=>'Listar Perfis',
'LBL_ALLOWED_MODULES'=>'Módulos permitidos: ',
'LBL_DISALLOWED_MODULES'=>'Módulos proibidos: ',
'LBL_NAME'=>'Nome: ',
'LNK_NEW_ROLE'=>'Novo Perfil',
'LBL_ROLE'=>'Perfil: ',
'LBL_DEFAULT_SUBPANEL_TITLE'=>'Perfis',
'LNK_ROLES'=>'Perfis',
'LBL_MODULE_NAME'=>'Perfis',
'LBL_MODULE_TITLE'=>'Perfis: Tela Principal',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar Perfis',
'LBL_USERS'=>'Usuários',
'LBL_USERS_SUBPANEL_TITLE'=>'Usuários',
);
?>