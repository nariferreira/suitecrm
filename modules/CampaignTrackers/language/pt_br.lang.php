<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_CAMPAIGN'=>'Campanha',
'LNK_CAMPAIGN_LIST'=>'Campanhas',
'LBL_SUBPANEL_TRACKER_KEY'=>'Chave',
'LBL_TRACKER_KEY'=>'Chave do Rastreador',
'LBL_EDIT_TRACKER_KEY'=>'Chave do Rastreador:',
'LBL_CREATED_BY'=>'Criado por',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DATE_ENTERED'=>'Data de Introdução',
'LBL_EDIT_LAYOUT'=>'Editar Layout',
'LBL_DELETED'=>'Excluído',
'LBL_ID'=>'Id',
'LBL_CAMPAIGN_ID'=>'Id da Campanha',
'LBL_EDIT_OPT_OUT'=>'Link de Opt-out?',
'LBL_MODIFIED_USER_ID'=>'Modificado por',
'LBL_SUBPANEL_TRACKER_NAME'=>'Nome',
'LBL_EDIT_CAMPAIGN_NAME'=>'Nome da Campanha:',
'LBL_TRACKER_NAME'=>'Nome do Rastreador',
'LBL_EDIT_TRACKER_NAME'=>'Nome do Rastreador:',
'LBL_OPTOUT'=>'Optou po Sair',
'LBL_MODULE_NAME'=>'Rastreador de Campanhas',
'LBL_SUBPANEL_TRACKER_URL'=>'URL',
'LBL_EDIT_MESSAGE_URL'=>'URL da Mensagem da Campanha:',
'LBL_TRACKER_URL'=>'URL do Rastreador',
'LBL_EDIT_TRACKER_URL'=>'URL do Rastreador:',
);
?>