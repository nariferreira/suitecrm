<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_FIELD'=>'Campo',
'LBL_MODULE_TITLE'=>'Condições do WorkFlow',
'LBL_MODULE_NAME'=>'Condições do WorkFlow',
'LBL_CREATED_USER'=>'Criado Por',
'LBL_CREATED'=>'Criado Por',
'LBL_CREATED_ID'=>'Criado Por',
'LBL_DATE_ENTERED'=>'Data da Criação',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DELETED'=>'Deletado',
'LBL_DESCRIPTION'=>'Descrição',
'LBL_ID'=>'ID',
'LBL_AOW_WORKFLOW_ID'=>'Id do WorkFlow',
'LBL_MODIFIED'=>'Modificado Por',
'LBL_MODIFIED_ID'=>'Modificado Por',
'LBL_MODIFIED_USER'=>'Modificado Por',
'LBL_MODIFIED_NAME'=>'Modificado Por',
'LBL_MODULE_PATH'=>'Módulo',
'LBL_NAME'=>'Nome',
'LBL_OPERATOR'=>'Operador',
'LBL_CONDITION_OPERATOR'=>'Operador da Condição',
'LBL_ORDER'=>'Ordem',
'LBL_VALUE_TYPE'=>'Tipo',
'LBL_VALUE'=>'Valor',
);
?>