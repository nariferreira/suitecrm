<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_MODULE_NAME'=>'Contas',
'LBL_MODULE_TITLE'=>'Contas: Tela Principal',
'LBL_LIST_FORM_TITLE'=>'Lista de Contas',
'LNK_NEW_NOTE'=>'Nova Anotação ou Anexo',
'LBL_NEW_FORM_TITLE'=>'Nova Conta',
'LNK_NEW_ACCOUNT'=>'Nova Conta',
'LNK_NEW_CALL'=>'Nova Ligação',
'LNK_NEW_CASE'=>'Nova Ocorrência',
'LNK_NEW_OPPORTUNITY'=>'Nova Oportunidade',
'LNK_NEW_MEETING'=>'Nova Reunião',
'LNK_NEW_TASK'=>'Nova Tarefa',
'LNK_NEW_CONTACT'=>'Novo Contato',
'LNK_NEW_EMAIL'=>'Novo E-mail',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar Contas',
'ERR_DELETE_RECORD'=>'Um número de registro deve ser especificado para excluir a Conta.',
);
?>