<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LNK_CONVERT_USER'=>'Converter o Usuário para o Grupo',
'LNK_NEW_GROUP'=>'Criar Novo Grupo',
'LBL_DESCRIPTION'=>'Descrição:',
'LBL_TEAM'=>'Equipe:',
'LBL_MODULE_NAME'=>'Grupos',
'LBL_LIST_TITLE'=>'Grupos',
'LBL_GROUP_NAME'=>'Nome do Grupo:',
'LNK_ALL_GROUPS'=>'Todos os Grupos',
);
?>