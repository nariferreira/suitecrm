<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_APPROVAL_ISSUE'=>'Assuntos Aprovados',
'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'Atividades',
'LBL_USERS_ASSIGNED_LINK'=>'Atribuído a',
'LBL_ASSIGNED_TO_ID'=>'Atribuído a',
'LBL_ASSIGNED_TO_NAME'=>'Atribuído a',
'LBL_SHIPPING_ADDRESS_POSTALCODE'=>'CEP de Entrega:',
'LBL_BILLING_ADDRESS_POSTALCODE'=>'CEP de Faturamento:',
'LBL_POSTAL_CODE'=>'CEP:',
'LBL_LIST_CITY'=>'Cidade',
'LBL_SHIPPING_ADDRESS_CITY'=>'Cidade de Entrega:',
'LBL_BILLING_ADDRESS_CITY'=>'Cidade de Faturamento:',
'LBL_CITY'=>'Cidade:',
'LBL_RATING'=>'Classificação',
'LBL_BILLING_ACCOUNT'=>'Conta',
'LBL_SHIPPING_ACCOUNT'=>'Conta de Entrega',
'LBL_DEFAULT_SUBPANEL_TITLE'=>'Contas',
'LNK_ACCOUNT_LIST'=>'Contas',
'LBL_BILLING_CONTACT'=>'Contato',
'LBL_SHIPPING_CONTACT'=>'Contato na Entrega',
'LBL_CONTACTS_SUBPANEL_TITLE'=>'Contatos',
'LBL_CONVERT_TO_INVOICE'=>'Converter a Pedido',
'NTC_COPY_SHIPPING_ADDRESS'=>'Copiar endereço de entrega para o endereço de faturamento',
'NTC_COPY_BILLING_ADDRESS'=>'Copiar endereço de faturamento para o endereço de entrega',
'NTC_COPY_BILLING_ADDRESS2'=>'Copiar para Entrega',
'NTC_COPY_SHIPPING_ADDRESS2'=>'Copiar para Faturamento',
'LBL_PDF_NAME'=>'Cotação',
'LBL_EMAIL_NAME'=>'Cotação para',
'LBL_MODULE_TITLE'=>'Cotações',
'LBL_AOS_INVOICES_SUBPANEL_TITLE'=>'Cotações',
'LBL_MODULE_NAME'=>'Cotações',
'LBL_AOS_QUOTES_SUBPANEL_TITLE'=>'Cotações',
'LBL_USERS_CREATED_LINK'=>'Criado Por',
'LBL_CREATED_ID'=>'Criado Por',
'LBL_CREATED_USER'=>'Criado Por',
'LBL_CREATED'=>'Criado Por',
'LNK_NEW_ACCOUNT'=>'Criar Conta',
'LBL_CREATE_CONTRACT'=>'Criar Contato',
'LNK_NEW_RECORD'=>'Criar Cotação',
'LBL_CREATE_OPPORTUNITY'=>'Criar Oportnidade',
'MSG_SHOW_DUPLICATES'=>'Criar esta conta pode incorrer em duplicidade. Você pode clicar em Salvar para criar a conta com os dados informados ou clicar em Cancelar.',
'MSG_DUPLICATE'=>'Criar esta conta pode incorrer em duplicidade. Você pode selecionar uma conta da lista abaixo ou pode clicar em Salvar para criar a conta com os dados informados.',
'LBL_PART_NUMBER'=>'Código do Produto',
'LBL_QUOTE_DATE'=>'Data Cotação',
'LBL_DATE_ENTERED'=>'Data da Criação',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DELETED'=>'Deletado',
'LBL_REMOVE_PRODUCT_LINE'=>'Deletar',
'LBL_DELETE_GROUP'=>'Deletar Grupo',
'LBL_DISCOUNT_AMT'=>'Desconto',
'LBL_DISCOUNT_AMOUNT'=>'Desconto',
'LBL_SERVICE_DISCOUNT'=>'Desconto',
'LBL_PRODUCT_DESCRIPTION'=>'Descrição',
'LBL_DESCRIPTION'=>'Descrição',
'LBL_NO_TEMPLATE'=>'ERRO\nNenhum formato encontrado. Se você não tem um formado definido, acesso o módulo de criação de formatos PDF e crie um.',
'LBL_ANY_EMAIL'=>'Email:',
'LBL_EMAIL'=>'Email:',
'LBL_EMPLOYEES'=>'Empregados:
',
'LBL_ACCOUNT'=>'Empresa:',
'LBL_BILLING_ADDRESS_STREET_2'=>'Endereço 2 de Faturamento:',
'LBL_BILLING_ADDRESS_STREET_3'=>'Endereço 3 de Faturamento:',
'LBL_BILLING_ADDRESS_STREET_4'=>'Endereço 4 de Faturamento:',
'LBL_LIST_EMAIL_ADDRESS'=>'Endereço de Email',
'LBL_SHIPPING_ADDRESS_STREET_2'=>'Endereço de Entrega 2:',
'LBL_SHIPPING_ADDRESS_STREET_3'=>'Endereço de Entrega 3:',
'LBL_SHIPPING_ADDRESS_STREET_4'=>'Endereço de Entrega 4:',
'LBL_SHIPPING_ADDRESS_STREET'=>'Endereço de Entrega:',
'LBL_SHIPPING_ADDRESS'=>'Endereço de Entrega:',
'LBL_BILLING_ADDRESS'=>'Endereço de Faturamento:',
'LBL_BILLING_ADDRESS_STREET'=>'Endereço de Faturamento:',
'LBL_ANY_ADDRESS'=>'Endereço:',
'LBL_SHIPPING_AMOUNT'=>'Entrega',
'LBL_EMAIL_QUOTE'=>'Enviar Cotação por Email',
'LBL_EMAIL_PDF'=>'Enviar PDF por Email',
'LBL_TEAMS_LINK'=>'Equipes',
'LBL_LIST_STATE'=>'Estado',
'LBL_SHIPPING_ADDRESS_STATE'=>'Estado de Entrega:',
'LBL_BILLING_ADDRESS_STATE'=>'Estado de Faturamento:',
'LBL_STATE'=>'Estado:',
'LBL_STAGE'=>'Etapas de Cotação',
'LBL_ANNUAL_REVENUE'=>'Faturamento Anual:',
'LBL_FAX'=>'Fax:',
'LBL_PHONE_FAX'=>'Fax:',
'LBL_TERM'=>'Forma de Pagamento',
'LBL_TERMS_C'=>'Formas',
'LBL_TEMPLATE_DDOWN_C'=>'Formatos de Cotação',
'LBL_ID'=>'ID',
'LBL_PARENT_ACCOUNT_ID'=>'ID da Conta Pai',
'LBL_IMPORT_LINE_ITEMS'=>'Importar Itens',
'LBL_VAT'=>'Imposto',
'LBL_SHIPPING_TAX'=>'Imposto de Entrega',
'LBL_SHIPPING_TAX_AMT'=>'Imposto de Entrega',
'LBL_TAX_AMOUNT'=>'Impostos',
'LBL_PRINT_AS_PDF'=>'Imprimir como PDF',
'LBL_INDUSTRY'=>'Indústria:
',
'LBL_ADDRESS_INFORMATION'=>'Informações da Conta',
'LBL_ACCOUNT_INFORMATION'=>'Informações da Cotação',
'LBL_PUSH_SHIPPING'=>'Inserir Entrega',
'LBL_PUSH_BILLING'=>'Inserir Faturamento',
'LBL_ADD_GROUP'=>'Inserir Grupo',
'LBL_ADD_PRODUCT_LINE'=>'Inserir Linha de Produto',
'LBL_ADD_SERVICE_LINE'=>'Inserir Linha de Serviço',
'LBL_LINE_ITEMS'=>'Itens',
'LBL_SERVICE_LIST_PRICE'=>'Lista',
'LBL_LIST_PRICE'=>'Lista',
'LBL_LIST_FORM_TITLE'=>'Lista de Cotações',
'LBL_MEMBER_ORG_FORM_TITLE'=>'Membro de Organizações',
'LBL_MEMBER_ORG_SUBPANEL_TITLE'=>'Membro de Organizações',
'LBL_MEMBER_OF'=>'Membro de:',
'LBL_HOMEPAGE_TITLE'=>'Minhas Cotações',
'LBL_MODIFIED_NAME'=>'Modificado Por',
'LBL_MODIFIED_ID'=>'Modificado Por',
'LBL_MODIFIED'=>'Modificado Por',
'LBL_USERS_MODIFIED_LINK'=>'Modificado Por',
'LBL_MODIFIED_USER'=>'Modificado Por',
'LBL_LIST_ACCOUNT_NAME'=>'Nome da Conta',
'LBL_OPPORTUNITY'=>'Nome da Oportunidade',
'LBL_GROUP_NAME'=>'Nome do Grupo',
'LBL_PRODUCT_NOTE'=>'Nota',
'LBL_NEW_FORM_TITLE'=>'Nova Cotação',
'LBL_LIST_NUM'=>'Num.',
'LBL_QUOTE_NUMBER'=>'Número da Cotação',
'LBL_INVOICE_NUMBER'=>'Número do Pedido',
'LBL_OTHER_EMAIL_ADDRESS'=>'Outro Email:',
'LBL_OTHER_PHONE'=>'Outro Telefone:',
'LBL_SHIPPING_ADDRESS_COUNTRY'=>'País de Entrega:',
'LBL_BILLING_ADDRESS_COUNTRY'=>'País de Faturamento:',
'LBL_COUNTRY'=>'País:',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar Cotações',
'LBL_DUPLICATE'=>'Possível Conta Duplicada',
'LBL_UNIT_PRICE'=>'Preço de Venda',
'LBL_SERVICE_PRICE'=>'Preço de Venda',
'LBL_PRODUCT_NAME'=>'Produto',
'LBL_OWNERSHIP'=>'Propriedade:',
'LBL_PRODUCT_QUANITY'=>'Quantidade',
'LBL_SAVE_ACCOUNT'=>'Salvar Conta',
'LBL_SERVICE_NAME'=>'Serviço',
'LBL_TICKER_SYMBOL'=>'Simbolo de Ticker:',
'LBL_APPROVAL_STATUS'=>'Status da Aprovação',
'LBL_INVOICE_STATUS'=>'Status do Pedido',
'LBL_SUBTOTAL_AMOUNT'=>'Subtotal',
'LBL_SUBTOTAL_TAX_AMOUNT'=>'Subtotal + Impostos',
'LBL_LIST_PHONE'=>'Telefone',
'LBL_PHONE_ALT'=>'Telefone Alternativo:',
'LBL_PHONE_OFFICE'=>'Telefone do Escritório:',
'LBL_PHONE'=>'Telefone:',
'LBL_ANY_PHONE'=>'Telefone:',
'NTC_DELETE_CONFIRMATION'=>'Tem certeza que quer deletar este registro?',
'NTC_REMOVE_ACCOUNT_CONFIRMATION'=>'Tem certeza que quer deletar este registro?',
'ACCOUNT_REMOVE_PROJECT_CONFIRM'=>'Tem certeza que quer remover esta conta deste projeto?',
'NTC_REMOVE_MEMBER_ORG_CONFIRMATION'=>'Tem certeza que quer remover este registro da organização membro?',
'LBL_DISCOUNT_TYPE'=>'Tipo',
'LBL_TYPE'=>'Tipo:',
'LBL_TOTAL_AMT'=>'Total',
'LBL_TOTAL_PRICE'=>'Total',
'LBL_GRAND_TOTAL'=>'Total Geral',
'LBL_GROUP_TOTAL'=>'Total do Grupo',
'LBL_ACCOUNT_NAME'=>'Título',
'VALUE'=>'Título',
'LBL_NAME'=>'Título',
'ERR_DELETE_RECORD'=>'Um número de registro dever ser especificado para deletar a conta.',
'LBL_ASSIGNED_USER'=>'Usuário',
'LBL_VAT_AMT'=>'Valor do Imposto',
'LBL_VIEW_FORM_TITLE'=>'Visualizar Contas',
'LNK_LIST'=>'Visualizar Cotações',
'LBL_HISTORY_SUBPANEL_TITLE'=>'Visualizar Histórico',
'LBL_EXPIRATION'=>'Válido até',
'LBL_LIST_WEBSITE'=>'Website',
'LBL_WEBSITE'=>'Website:',
);
?>