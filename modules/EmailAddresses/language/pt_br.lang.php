<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_EMAIL_ADDRESS_CAPS'=>'Caps Endereço de E-mail',
'LBL_DATE_CREATE'=>'Data da Criação',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_INVALID_EMAIL'=>'E-mail Inválido',
'LBL_EMAIL_ADDRESS'=>'Endereço de E-mail',
'LBL_DELETED'=>'Excluir',
'LBL_EMAIL_ADDRESS_ID'=>'ID',
'LBL_OPT_OUT'=>'Optou por Sair',
);
?>