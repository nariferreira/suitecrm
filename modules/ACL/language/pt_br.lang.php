<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_ADDING'=>'Adicionar para',
'LBL_DESCRIPTION'=>'Descrição',
'LIST_ROLES'=>'Listar Perfis',
'LIST_ROLES_BY_USER'=>'Listar Perfis por Usuário',
'LBL_ALLOW_NONE'=>'Nenhum',
'LBL_NAME'=>'Nome',
'LBL_ROLE'=>'Perfil',
'LBL_ROLES_SUBPANEL_TITLE'=>'Perfis do Usuário',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar',
'LBL_ALLOW_OWNER'=>'Proprietário',
'LBL_REDIRECT_TO_HOME'=>'Redirecionar para Home em',
'LBL_ALLOW_ALL'=>'Todos',
'LBL_USERS_SUBPANEL_TITLE'=>'Usuários',
'LBL_NO_ACCESS'=>'Você não tem acesso a esta área. Por favor, contate o administrador do sistema para obter acesso.',
'LBL_SECONDS'=>'segundos',
);
?>