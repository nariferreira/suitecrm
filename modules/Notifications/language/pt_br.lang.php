<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_HOMEPAGE_TITLE'=>'As Minhas Notificações',
'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'Atividades',
'LBL_ASSIGNED_TO_NAME'=>'Atribuído a',
'LBL_ASSIGNED_TO_ID'=>'Atribuído a',
'LBL_CREATED'=>'Criado Por',
'LBL_CREATED_USER'=>'Criado pelo Usuário',
'LBL_DATE_ENTERED'=>'Data da Criação',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DELETED'=>'Deletado',
'LBL_DESCRIPTION'=>'Descrição',
'LBL_TEAM'=>'Equipe',
'LBL_TEAMS'=>'Equipes',
'LBL_ID'=>'ID',
'LBL_TEAM_ID'=>'Id Equipe',
'LBL_CREATED_ID'=>'Id de Criado Por',
'LBL_MODIFIED_ID'=>'Id de Modificado Por',
'LBL_IS_READ'=>'Lido',
'LBL_LIST_FORM_TITLE'=>'Lista de Notificações',
'LBL_MODIFIED'=>'Modificado Por',
'LBL_MODIFIED_NAME'=>'Modificado Por Nome',
'LBL_MODIFIED_USER'=>'Modificado pelo Usuário',
'LBL_NAME'=>'Nome',
'LBL_ASX_NOTIFICATIONS_SUBPANEL_TITLE'=>'Notificações',
'LBL_MODULE_TITLE'=>'Notificações',
'LBL_MODULE_NAME'=>'Notificações',
'LNK_LIST'=>'Notificações',
'LNK_NEW_RECORD'=>'Nova Notificação',
'LBL_NEW_FORM_TITLE'=>'Novas Notificações',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar Notificações',
'LBL_HISTORY_SUBPANEL_TITLE'=>'Ver Histórico',
);
?>