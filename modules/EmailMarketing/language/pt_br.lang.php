<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_FROM_MAILBOX'=>'Caixa de Correio De',
'LBL_MODULE_SEND_EMAILS'=>'Campanha: Enviar Emails',
'LBL_MODULE_SEND_TEST'=>'Campanha: Enviar Teste',
'LNK_CAMPAIGN_LIST'=>'Campanhas',
'LBL_LIST_FORM_TITLE'=>'Campanhas de Marketing por Email',
'LBL_CREATED_BY'=>'Criado por:',
'LBL_CREATED'=>'Criado por:',
'LBL_CREATE_EMAIL_TEMPLATE'=>'Criar',
'LBL_START_DATE_TIME'=>'Data & Hora de Início:',
'LBL_DATE_CREATED'=>'Data da Criação:',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DATE_LAST_MODIFIED'=>'Data da Modificação:',
'LBL_DATE_ENTERED'=>'Data de Introdução',
'LBL_LIST_DATE_START'=>'Data de Início',
'LBL_DATE_START'=>'Data de Início',
'LBL_EDIT_EMAIL_TEMPLATE'=>'Editar',
'LBL_LIST_FROM_ADDR'=>'Email do Remetente',
'LBL_FROM_ADDR'=>'Endereço De:',
'LBL_SCHEDULE_BUTTON_TITLE'=>'Enviar',
'LBL_SCHEDULE_BUTTON_LABEL'=>'Enviar',
'LBL_MESSAGE_FOR'=>'Enviar Esta Mensagem Para:',
'LBL_TIME_START'=>'Hora de Início',
'LBL_LIST_PROSPECT_LIST_NAME'=>'Listas de Prospectos',
'LBL_PROSPECT_LIST_SUBPANEL_TITLE'=>'Listas de Prospectos',
'LNK_PROSPECT_LIST_LIST'=>'Listas de Prospectos',
'LBL_MODULE_NAME'=>'Marketing por Email',
'LBL_DEFAULT_SUBPANEL_TITLE'=>'Marketing por Email',
'LBL_MODULE_TITLE'=>'Marketing por Email: Tela Principal',
'LBL_MESSAGE_FOR_ID'=>'Mensagem Para',
'LBL_LIST_TEMPLATE_NAME'=>'Modelo de Email',
'LBL_TEMPLATE'=>'Modelo de Email:',
'LBL_MODIFIED'=>'Modificado por:',
'LBL_MODIFIED_BY'=>'Modificado por:',
'LBL_LIST_NAME'=>'Nome',
'LBL_FROM_NAME'=>'Nome De:',
'LBL_PROSPECT_LIST_NAME'=>'Nome da Lista de Prospectos',
'LBL_TEMPLATE_NAME'=>'Nome de Modelo',
'LBL_NAME'=>'Nome:',
'LNK_NEW_CAMPAIGN'=>'Nova Campanha',
'LNK_NEW_PROSPECT_LIST'=>'Nova Lista de Prospectos',
'LNK_NEW_PROSPECT'=>'Novo Prospecto',
'LBL_SCHEDULE_MESSAGE_EMAILS'=>'Por favor, selecione as mensagens de campanha para as quais gostaria de agendar o envio em determinada data e hora:',
'LBL_SCHEDULE_MESSAGE_TEST'=>'Por favor, selecione as mensagens de campanha que gostaria de enviar como teste:',
'LNK_PROSPECT_LIST'=>'Prospectos',
'LBL_REPLY_ADDR'=>'Responder ao Endereço:',
'LBL_REPLY_NAME'=>'Responder ao Nome:',
'LBL_LIST_STATUS'=>'Status',
'LBL_STATUS'=>'Status',
'LBL_STATUS_TEXT'=>'Status:',
'LBL_SCHEDULE_BUTTON_KEY'=>'T',
'LBL_ALL_PROSPECT_LISTS'=>'Todas as Listas de Prospectos na Campanha.',
'LBL_RELATED_PROSPECT_LISTS'=>'Todas as Listas de Prospectos relacionadas a esta mensagem.',
'LBL_FROM_MAILBOX_NAME'=>'Usar Caixa de Correio:',
);
?>