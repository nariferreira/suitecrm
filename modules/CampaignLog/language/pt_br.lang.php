<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_MODIFIED'=>'Alterado por:',
'LBL_ARCHIVED'=>'Arquivado',
'LBL_ASSIGNED_TO'=>'Atribuído a:',
'LBL_LIST_CAMPAIGN_NAME'=>'Campanha',
'LBL_CAMPAIGN'=>'Campanha:',
'LBL_CAMPAIGNS'=>'Campanhas',
'LBL_LIST_FORM_TITLE'=>'Campanhas',
'LBL_TARGET_TRACKER_KEY'=>'Chave do Rastreador de Prospectos',
'LBL_CLICKED_URL_KEY'=>'Chave do URL Clicado',
'LBL_CREATED_CONTACT'=>'Contato Criado',
'LBL_INVITEE'=>'Contatos',
'LBL_CREATED_OPPORTUNITY'=>'Criada uma Nova Oportunidade',
'LBL_CREATED'=>'Criado por:',
'LBL_CAMPAIGN_EXPECTED_COST'=>'Custo Estimado:',
'LBL_CAMPAIGN_ACTUAL_COST'=>'Custo Real:',
'LBL_LIST_END_DATE'=>'Data Final',
'LBL_CAMPAIGN_END_DATE'=>'Data Final:',
'LBL_CAMPAIGN_START_DATE'=>'Data Inicial:',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_LIST_ACTIVITY_DATE'=>'Data de Atividade',
'LBL_ACTIVITY_DATE'=>'Data de Atividade',
'LBL_DATE_ENTERED'=>'Data de Introdução',
'LBL_DELETED'=>'Deletado',
'LBL_CAMPAIGN_CONTENT'=>'Descrição:',
'LBL_LIST_MARKETING_NAME'=>'Email Enviado',
'LBL_SENT_EMAIL'=>'Email Enviado',
'LBL_LIST_RECIPIENT_EMAIL'=>'Email do Destinatário',
'LBL_TEAM'=>'Equipe:',
'LBL_HITS'=>'Hits',
'LBL_ID'=>'ID',
'LBL_LIST_ID'=>'ID da Lista de Potenciais Clientes',
'LBL_TARGET_ID'=>'ID do Prospecto',
'LBL_RELATED_ID'=>'Id Relacionado',
'LBL_MORE_INFO'=>'Mais Informações',
'LBL_LIST_RECIPIENT_NAME'=>'Nome do Destinatário',
'LBL_CAMPAIGN_NAME'=>'Nome:',
'LBL_NAME'=>'Nome:',
'LBL_LIST_CAMPAIGN_OBJECTIVE'=>'Objetivo da Campanha',
'LBL_CAMPAIGN_OBJECTIVE'=>'Objetivo:',
'LBL_CAMPAIGN_BUDGET'=>'Orçamento:',
'LBL_CREATED_LEAD'=>'Potencial Criado',
'LBL_CAMPAIGN_EXPECTED_REVENUE'=>'Receita Estimada:',
'LBL_MODULE_NAME'=>'Registro de Campanha',
'LBL_RELATED'=>'Relacionado',
'LBL_LIST_STATUS'=>'Status',
'LBL_CAMPAIGN_STATUS'=>'Status:',
'LBL_LIST_TYPE'=>'Tipo',
'LBL_RELATED_TYPE'=>'Tipo Relacionado',
'LBL_ACTIVITY_TYPE'=>'Tipo de Atividade',
'LBL_TARGET_TYPE'=>'Tipo de Prospecto',
'LBL_CAMPAIGN_TYPE'=>'Tipo:',
'LBL_URL_CLICKED'=>'URL Clicado',
'LBL_TARGETED_USER'=>'Usuário Destino',
);
?>