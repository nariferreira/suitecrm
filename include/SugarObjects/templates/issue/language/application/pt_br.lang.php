<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $app_list_strings = array ( 
 '_priority_dom' => array ( 
''=>'',
'P1'=>'Alta',
'P3'=>'Baixa',
'P2'=>'Média',
),
 '_resolution_dom' => array ( 
''=>'',
''=>'-Nenhuma-',
'Accepted'=>'Aceite',
'Duplicate'=>'Duplicar',
'Closed'=>'Fechado',
'Invalid'=>'Inválido',
'Out of Date'=>'Obsoleto',
),
 '_status_dom' => array ( 
''=>'',
'Pending Input'=>'Aguardando Resposta',
'Assigned'=>'Atribuído',
'Duplicate'=>'Duplicar',
'Closed'=>'Fechado',
'New'=>'Novo',
'Rejected'=>'Rejeitado',
),
 '_type_dom' => array ( 
''=>'',
'Administration'=>'Administração',
'Product'=>'Produto',
'User'=>'Usuário',
),
);
?>