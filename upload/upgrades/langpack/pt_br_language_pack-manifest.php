<?php
$manifest = array( 
	'name' => '250914 - Language Pack',
    'description' => 'Language Pack 250914 - <a href="http://www.cbrconsultoria.com.br" target="_blank">CBR Consultoria</a>',
	'type' => 'langpack',
	'is_uninstallable' => 'Yes',
	'version' => '7.1.1',
	'acceptable_sugar_flavors' => array("CE"),
	'author' => 'CBR Consultoria',
	'acceptable_sugar_versions' => array("regex_matches" => array("6.5.[0-9]")),
	'published_date' => '2014-09-25',
);

$installdefs = array(
	'id' => 'pt_br',
	'image_dir' => '<basepath>/images',
	'copy' => array(
		array('from' => '<basepath>/translates/include', 'to' => 'include',),
		array('from' => '<basepath>/translates/modules', 'to' => 'modules'),
		array('from' => '<basepath>/translates/install', 'to' => 'install'),
	),
);
?>
