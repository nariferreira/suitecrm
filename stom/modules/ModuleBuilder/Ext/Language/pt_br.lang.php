<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_COPY_FROM'=>'Copiar De:',
'LBL_DEFAULT'=>'Default',
'LBL_SECURITYGROUP'=>'Grupo de Segurança:',
'LBL_ADD_LAYOUTS'=>'Inserir Layout',
'LBL_ADD_LAYOUT'=>'Inserir Layout',
'LBL_REMOVELAYOUTDONE'=>'Layout Removido',
'LBL_ADDLAYOUTDONE'=>'Layout Salvo',
'LBL_REMOVE_LAYOUT'=>'Remover Layout de Grupo',
'LBL_QUESTION_ADD_LAYOUT'=>'Selecione um Layout de Grupo para Inserir.',
'LBL_REMOVE_CONFIRM'=>'Você tem certeza?',
 'help' => array ( 
''=>'',
'studioWizard'=>'',
),
 'fieldTypes' => array ( 
''=>'',
'dynamicenum'=>'DropDown Dinâmico',
),
);
?>