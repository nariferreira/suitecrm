<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_UPDATE_MSG'=>'<strong>Atualização da Aplicação para QuickCRM no Mobile</strong><br/>Você pode acessar a versão mobile em:',
'LBL_SALESAGILITY_ADMIN'=>'Advanded OpenAdmin',
'LBL_ERR_DIR_MSG'=>'Alguns arquivos não foram criados. Favor checar as permissões de gravação para:',
'LBL_CHANGE_SETTINGS'=>'Alterar Configurações do Advanced OpenSales',
'LBL_JJWG_MAPS_ADMIN_CONFIG_DESC'=>'As definições de configuração para ajustar seu Google Mapas',
'LBL_AOD_ENABLE'=>'Ativa AOD',
'LBL_UPDATE_QUICKCRM_TITLE'=>'Atualização do QuickCRM',
'LBL_AOP_JOOMLA_ACCESS_KEY'=>'Chave de Acesso do Joomla',
'LBL_RESCHEDULE_ADMIN_DESC'=>'Configurar e Manter o Reagendamento',
'LBL_DEFAULT_ADMIN_DASHLET'=>'Configuração da Coluna de Dashlet',
'LBL_COLOUR_ADMIN_PAGE'=>'Configuração da Cor da Página',
'LBL_COLOUR_ADMIN_BUTTON'=>'Configuração da Cor do Botão',
'LBL_COLOUR_ADMIN_DASHLET'=>'Configuração da Cor do Dashlet',
'LBL_COLOUR_ADMIN_MENU'=>'Configuração da Cor do Menu',
'LBL_AOS_ADMIN_QUOTE_SETTINGS'=>'Configuração das Cotações',
'LBL_AOS_ADMIN_CONTRACT_SETTINGS'=>'Configuração de Contratos',
'LBL_COLOUR_ADMIN_CUSTOM'=>'Configuração de Código Customizado',
'LBL_AOP_EMAIL_SETTINGS'=>'Configuração de Email',
'LBL_AOS_ADMIN_LINE_ITEM_SETTINGS'=>'Configuração de Itens de Linha',
'LBL_AOS_ADMIN_INVOICE_SETTINGS'=>'Configuração de Pedidos',
'LBL_AOP_SETTINGS'=>'Configuração do AOP',
'LBL_AOS_SETTINGS'=>'Configuração do AOS',
'LBL_AOD_ADMIN_MANAGE_AOD'=>'Configuração do Advanced OpenDiscovery',
'LBL_AOP_ADMIN_MANAGE_AOP'=>'Configuração do Advanced OpenPortal',
'LBL_AOS_ADMIN_MANAGE_AOS'=>'Configuração do Advanced OpenSales',
'LBL_COLOUR_ADMIN_TABS'=>'Configuração do Menu:',
'LBL_AOP_JOOMLA_SETTINGS'=>'Configuração do Portal',
'LBL_CONFIG_QUICKCRM_TITLE'=>'Configuração do QuickCRM',
'LBL_RESCHEDULE_ADMIN'=>'Configuração do Reagendamento',
'LBL_COLOUR_SETTINGS'=>'Configuração do Tema',
'LBL_AOD_SETTINGS'=>'Configurações do AOD',
'LBL_CONFIG_SECURITYGROUPS'=>'Configurações do Gerenciador do Módulo de Grupos de Segurança como herança, aditivo de segurança, etc',
'LBL_JJWG_MAPS_ADMIN_CONFIG_TITLE'=>'Configurações do Google Mapas',
'LBL_CONFIG_SECURITYGROUPS_TITLE'=>'Configurações do Módulo de Grupos de Segurança',
'LBL_COLOUR_ADMIN_MENUBRD'=>'Cor da Borda do Menu:',
'LBL_COLOUR_ADMIN_TOPGR'=>'Cor da Gradiente Superior',
'LBL_COLOUR_ADMIN_BTMGR'=>'Cor da Gradiente do Botão',
'LBL_COLOUR_ADMIN_PAGELINK'=>'Cor da Página de Link',
'LBL_COLOUR_ADMIN_MENUHOVER'=>'Cor de Foco do Menu:',
'LBL_COLOUR_ADMIN_BTNHOVER'=>'Cor do Botão:',
'LBL_COLOUR_ADMIN_DDLINK'=>'Cor do Link de Drop down',
'LBL_COLOUR_ADMIN_MENULNKHVR'=>'Cor do Link de Menu em Foco:',
'LBL_COLOUR_ADMIN_MENUFONT'=>'Cor do Link do Menu',
'LBL_COLOUR_ADMIN_VISITED'=>'Cor do Link visitado:',
'LBL_COLOUR_ADMIN_BASE'=>'Cor do Menu Base:',
'LBL_COLOUR_ADMIN_DDMENU'=>'Cor do Menu do Drop Down:',
'LBL_COLOUR_ADMIN_PAGEHEADER'=>'Cor do cabeçalho da página',
'LBL_COLOUR_ADMIN_DASHHEAD'=>'Cor do cabeçalho do Dashlet',
'LBL_COLOUR_ADMIN_BTNMID2'=>'Cor do mid-bottom do Botão:',
'LBL_COLOUR_ADMIN_BTNMID1'=>'Cor do mid-top do Botão:',
'LBL_COLOUR_ADMIN_BTNBTM'=>'Cor do rodapé do Botão',
'LBL_COLOUR_ADMIN_BTNTOP'=>'Cor do topo do botão',
'LBL_JJWG_MAPS_ADMIN_GEOCODED_COUNTS_TITLE'=>'Counts Georreferenciados',
'LBL_CREATE_EMAIL_TEMPLATE'=>'Criar',
'LBL_COLOUR_DESC'=>'Customize o Tema do SuiteCRM',
'LBL_UPDATE_QUICKCRM'=>'Definição dos Campos de Reconstrução',
'LBL_CONFIG_QUICKCRM'=>'Definição dos campos e módulos visíveis',
'LBL_SECURITYGROUPS_SUGAROUTFITTERS'=>'Descarga la última versión de SecuritySuite y encontrar otros módulos SugarCRM, temas e integraciones con opiniones, documentos, soporte y versiones verificadas de la comunidad.',
'LBL_AOP_OPENING_DAYS'=>'Dias de Abertura',
'LBL_AOP_CASE_ASSIGNMENT'=>'Distribuição de Casos',
'LBL_JJWG_MAPS_ADMIN_DONATE_TITLE'=>'Doações para este Projeto',
'LBL_EDIT_EMAIL_TEMPLATE'=>'Editar',
'LBL_AOS_EDIT'=>'Editar',
'LBL_MANAGE_SECURITYGROUPS'=>'Editor do Módulo de Grupos de Segurança',
'LBL_SUPPORT_FROM_ADDRESS'=>'Email de Suporte do Endereço',
'LBL_SUPPORT_FROM_NAME'=>'Email de Suporte do Nome',
'LBL_SECURITYGROUPS_DASHLETPUSH_TITLE'=>'Empurre a mensagem Dashlet',
'LBL_JJWG_MAPS_ADMIN_ADDRESS_CACHE_TITLE'=>'Endereço Cache',
'LBL_JJWG_MAPS_ADMIN_GEOCODE_ADDRESSES_TITLE'=>'Endereços Geocode',
'LBL_JJWG_MAPS_ADMIN_GEOCODING_TEST_DESC'=>'Executar um único teste de geocodificação com resultados exibição detalhada.',
'LBL_AOP_BUSINESS_HOURS_SETTINGS'=>'Expediente',
'LBL_JJWG_MAPS_ADMIN_ADDRESS_CACHE_DESC'=>'Fornece acesso a informações de endereço Cache. Esta é apenas uma memória cache.',
'LBL_JJWG_MAPS_ADMIN_GEOCODE_ADDRESSES_DESC'=>'Geocode seus addreses objeto. Este processo pode demorar alguns minutos!',
'LBL_MANAGE_SECURITYGROUPS_TITLE'=>'Gerenciamento do Módulo de Grupos de Segurança',
'LBL_JJWG_MAPS_ADMIN_DESC'=>'Gerenciar seu geocodificação, geocodificação testar, ver geocodificação totais resultado e definir configurações avançadas.',
'LBL_JJWG_MAPS_ADMIN_HEADER'=>'Google Mapas',
'LBL_AOS_ADMIN_ENABLE_LINE_ITEM_GROUPS'=>'Habilitar Grupo de Itens de Linha',
'LBL_AOP_ENABLE_PORTAL'=>'Habilitar Portal',
'LBL_AOP_OPENING_HOURS'=>'Horas de Abertura',
'LBL_AOP_CLOSING_HOURS'=>'Horas de Fechamento',
'LBL_SECURITYGROUPS_INFO'=>'Informação Geral',
'LBL_SECURITYGROUPS_INFO_TITLE'=>'Informação Grupos de Segurança',
'LBL_SECURITYGROUPS_UPGRADE_INFO_TITLE'=>'Melhoramento e Informação Geral',
'LBL_AOP_JOOMLA_ACCOUNT_CREATION_EMAIL_TEMPLATE'=>'Modelo de Criação de Conta para o Portal Joomla',
'LBL_AOP_CONTACT_EMAIL_TEMPLATE'=>'Modelo de Email de Contato',
'LBL_AOP_USER_EMAIL_TEMPLATE'=>'Modelo de Email do Usuário',
'LBL_AOP_CASE_REMINDER_EMAIL_TEMPLATE'=>'Modelo de Email para Lembrete de Ocorrências',
'LBL_AOP_CASE_CREATION_EMAIL_TEMPLATE'=>'Modelo de Email para Ocorrência Criadas',
'LBL_AOP_CASE_CLOSURE_EMAIL_TEMPLATE'=>'Modelo de Email para Ocorrências Encerradas',
'LBL_JJWG_MAPS_ADMIN_GEOCODED_COUNTS_DESC'=>'Mostra o número de objetos do módulo geocodificados, agrupados por resposta de geocodificação.',
'LBL_COLOUR_ADMIN_INTRO'=>'Mude esta configuração para customizar o tema do SuiteCRM. <strong>Nota:</strong>Lembre-se de limpar o cache (CTRL + F5) após salvar a configuração.',
'LBL_AOP_DISTRIBUTION_METHOD'=>'Método de Distribuição',
'LBL_SECURITYGROUPS_HOOKUP_TITLE'=>'Módulo da conexão',
'LBL_SECURITYGROUPS'=>'Módulo de Grupos de Segurança',
'LBL_AOS_ADMIN_INITIAL_QUOTE_NUMBER'=>'Número Inicial da Cotação',
'LBL_AOS_ADMIN_INITIAL_INVOICE_NUMBER'=>'Número Inicial do Pedido',
'LBL_DASHLET_COLUMNS'=>'Número de Colunas do Dashlet',
'LBL_COLOUR_ADMIN_TABSNUM'=>'Número de itens/tabs do Menu:',
'LBL_AOS_ADMIN_CONTRACT_RENEWAL_REMINDER'=>'Período de Lembrete de Renovação',
'LBL_JJWG_MAPS_ADMIN_DONATE_DESC'=>'Por favor, considere fazer uma doação para este projeto!',
'LBL_AOS_PRODUCTS'=>'Produtos AOS',
'LBL_SECURITYGROUPS_DASHLETPUSH'=>'Push the Message Dashlet to the Home page for all users. This process may take some time to complete depending on the number of users',
'LBL_QUICKCRM'=>'QuickCRM Mobile',
'LBL_REPAIR_RESCHEDULE_DONE'=>'Reagendamento reparado com sucesso',
'LBL_RESCHEDULE_REBUILD'=>'Reparar Reagendamento',
'LBL_RESCHEDULE_REBUILD_DESC'=>'Reparar o Módulo de Reagendamento',
'LBL_AOS_ADMIN_ENABLE_LINE_ITEM_TOTAL_TAX'=>'Somar Imposto ao Total da Linha',
'LBL_SECURITYGROUPS_SUGAROUTFITTERS_TITLE'=>'SugarOutfitters',
'LBL_SECURITYGROUPS_HOOKUP'=>'Série da Grupos de Segurança a trabalhar com seus módulos feitos sob encomenda',
'LBL_JJWG_MAPS_ADMIN_GEOCODING_TEST_TITLE'=>'Teste de Geocodificação',
'LBL_AOP_JOOMLA_URL'=>'URL do Joomla',
'LBL_AOP_ASSIGNMENT_USER'=>'Usuário de Distribuição',
'LBL_SINGLE_USER'=>'Usuário Único',
'LBL_AOS_DAYS'=>'dias',
);
?>